
tau = 2*pi;

%% Wavelet parameters
    gaussianBandwidth = 1;  % bandwidth parameter (how wide the gaussian window is)
    morletFrequency = tau; % wavelet center frequency (how wavey the e^it is)

%% Signal Parameters
    
    sampling_rate = 60;
    
    signalamp1 = 1;
    signalfreq1 = 1;
    signaltime1 = 3;
    signalamp2 = 0.5;
    signalfreq2 = 4;
    signaltime2 = 3;
    signalamp3 = 1;
    signalfreq3 = 2.5;
    signaltime3 = 5;
    
signal1 = signalamp1*sin(1:tau*signalfreq1/sampling_rate:tau*signalfreq1*signaltime1);
signal2 = signalamp2*sin(1:tau*signalfreq2/sampling_rate:tau*signalfreq2*signaltime2);
signal3 = signalamp3*sin(1:tau*signalfreq3/sampling_rate:tau*signalfreq3*signaltime3);

totaltime = signaltime1 + signaltime2 + signaltime3;

signal = cat(2, signal1, signal2, signal3);

%% matlab calculations for comparison
matlabcwt = cwt(signal,'amor');
matlabrecovery = icwt(matlabcwt, 'amor');

% plot a simplified version because mesh is slow
figure(1);
[x, y] = size(matlabcwt);
smallerml = matlabcwt(1:5:x, 1:30:y);
mesh(abs(smallerml))

title("MATLAB CWT of a sine wave (w/o phase)");

%% cwt

    minScale = 2;    % the highest representable frequency, as fraction of sampling time
    maxScale = 120;  % the lowest representable frequency; higher is better, but slower
    scaleGrid = 3;   % the size of the frequency grid; lower is better, but slower
    timeGrid = 5;    % the size of the time grid; lower is better, but slower

wavelet = @(t) morlet(gaussianBandwidth, morletFrequency, t);

dx = 0.025; % arbitrary small value to measure wavelet mass with
waveletMass = @(from, to) sum(abs(arrayfun(wavelet, max(from, -4):dx:min(to, 4))))*dx;
totalwaveletMass = waveletMass(-4, 4); % integral from -inf to inf of the wavelet
    
x = ceil((maxScale - minScale + 1) / scaleGrid);
y = ceil(length(signal) / timeGrid);
mycwt = zeros(x, y);

normalizationconstant = 2;
% All I know is the normalizationConstant does not depend on the morlet
% center frequency, and depends linearly on the gaussianBandwidth.

o = 0;
for offset=1:timeGrid:length(signal)
    o = o + 1;
    s = 0;
    for scale=minScale:scaleGrid:maxScale
        s = s + 1;
        
        % the edgeNormalization component is near 1 for most of the TFR.
        % But near the start and the end of the signal, it helps prevent
        % the TFR from "dropping off" to about 0.5*amp
        
        % I'm not sure why, but offset+4 or offset+5 does a much better job
        % at this normalization than offset or offset+1. This should be
        % investigated.
        localwaveletMass = waveletMass(-(offset+4)/scale, (length(signal)-offset)/scale);
        edgeNormalization = totalwaveletMass / localwaveletMass;
        
        for t=1:length(signal)
            m = wavelet((t - offset) / scale);
            mycwt(s, o) = mycwt(s, o) + m * signal(t);
        end
        mycwt(s, o) = mycwt(s, o) * edgeNormalization / scale;
    end
end
mycwt = mycwt * normalizationconstant;

figure(2);
mesh(abs(mycwt));

[myfreqsamples, mysizesamples] = size(mycwt);
maxfreq = sampling_rate / minScale;
minfreq = sampling_rate / maxScale;

title("Custom CWT of a sine wave (w/o phase)");

ylabel('f[Hz]');
xlabel('t[s]');
xt = get(gca, 'XTick');
yt = get(gca, 'YTick');
set(gca, 'XTick', xt, 'XTickLabel', xt/mysizesamples * totaltime);
set(gca , 'YTick', yt, 'YTickLabel', sampling_rate ./ (yt / max(yt) * (maxScale - minScale) + minScale));

%% icwt

dualwavelet = @(t) conj(morlet(gaussianBandwidth, morletFrequency, t));

invnormconstant = sqrt(2*pi); % todo: make sure it is correct

recovered = zeros(1,length(signal));
o = 0;
for offset=1:timeGrid:length(signal)
    o = o + 1;
    s = 0;
    for scale=minScale:scaleGrid:maxScale
        s = s + 1;
        for t=1:length(signal)
            m = dualwavelet((t - offset) / scale);
            recovered(t) = recovered(t) + m * mycwt(s,o) / (scale^2);
        end
    end
end
recovered = recovered * timeGrid * scaleGrid * invnormconstant;

figure(3);
subplot(2,3,1);
plot(real(recovered));
title("Real");
subplot(2,3,2);
plot(imag(recovered));
title("Im");
subplot(2,3,3);
plot(abs(recovered));
title("Abs");
subplot(2,3,4);
plot(signal);
title("Original signal");
subplot(2,3,5);
plot(matlabrecovery);
title("Matlab ideal");
subplot(2,3,6);
plot(angle(recovered));
title("Phase");

figure(4);
plot3(real(recovered), imag(recovered), 1:length(recovered));
xlabel("Re");
ylabel("Im");
zlabel("Time");
title("Recovered signal");

figure(5);
clf('reset');
hold on;
plot(signal);
plot(real(recovered));
plot(matlabrecovery);
hold off;
legend('original', 'recovered', 'matlab');