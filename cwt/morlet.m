function [result] = morlet(Fb, Fc, t)
%MORLET wavelet 

%% Playing with these two parameters determines
% our frequency-certainty vs time-certainty

% a larger Fc*Fb gives us better frequency-certainty
% a smaller Fc*Fb gives us better time-certainty

% modifying Fc changes the output's frequency scale, so Fb changes are
% safer.

%%
    result = ((pi*Fb)^(-0.5))*exp(-((t^2/Fb))) * exp(1i*Fc*t);
    
end

